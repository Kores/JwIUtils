/*
 *      JwIUtils - Java utilities library <https://github.com/JonathanxD/JwIUtils>
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2022 TheRealBuggy/JonathanxD (https://github.com/JonathanxD/) <jonathan.scripter@programmer.net>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.github.jonathanxd.iutils.box;

import org.jetbrains.annotations.NotNull;

import java.io.*;

/**
 * <p>Reads values from a {@link IMutableBox} of {@link String}, this uses a {@link StringReader} under the hood.</p>
 * <br>
 * <p>If the value of the {@link IMutableBox box} changes, those changes will not be reflected in the {@link StringReader},
 * because this would cause unexpected inconsistencies in the reading code, such as when the string shrinks (and
 * the reader expects a larger string, because it knows from which box it came from),
 * or grows (and the reader expects a smaller string, also because it knows from which box it came from),
 * or entirely changes (and the reader is reading encoded data such as configurations or base64).</p>
 * <br>
 * <p>Beware that this {@link Reader} follows the same implementation implications of {@link StringReader},
 * {@link #close() closing} it causes the original {@link #sr StringReader} to be closed as well, and
 * further calls to {@link #read(char[], int, int)} will throw an {@link IOException}. Also, this {@link Reader}
 * supports {@link #reset()}, {@link #mark(int)}, {@link #skip(long)} and {@link #ready()}.</p>
 */
public class StringBoxReader extends Reader {

    private final StringReader sr;

    public StringBoxReader(@NotNull IMutableBox<String> box) {
        this.sr = new StringReader(box.get());
    }

    @Override
    public int read(@NotNull char[] cbuf, int off, int len) throws IOException {
        return this.sr.read(cbuf, off, len);
    }

    @Override
    public void close() throws IOException {
        this.sr.close();
    }

    @Override
    public void reset() throws IOException {
        this.sr.reset();
    }

    @Override
    public int read() throws IOException {
        return this.sr.read();
    }

    @Override
    public long skip(long n) throws IOException {
        return this.sr.skip(n);
    }

    @Override
    public boolean ready() throws IOException {
        return this.sr.ready();
    }

    @Override
    public boolean markSupported() {
        return this.sr.markSupported();
    }

    @Override
    public void mark(int readAheadLimit) throws IOException {
        this.sr.mark(readAheadLimit);
    }
}
