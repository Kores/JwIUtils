/*
 *      JwIUtils - Java utilities library <https://github.com/JonathanxD/JwIUtils>
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2022 TheRealBuggy/JonathanxD (https://github.com/JonathanxD/) <jonathan.scripter@programmer.net>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.github.jonathanxd.iutils.box;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * <p>Writes values to a {@link IMutableBox} of {@link String}, this uses a {@link StringWriter} under the hood.</p>
 * <br>
 * <p>The {@link #box} is only updated when this {@link Writer} is either {@link #flush() flushed} or {@link #close() closed},
 * this ensures that if the {@link #box} has changed while the data was being written, the new value is still upstreamed
 * to the {@link #box}, also, this avoids inconsistencies that may happen when updating the {@link #box} in the middle of the
 * write process and the resulting data being mixed and unpredictable.</p>
 * <br>
 * <p>Beware that this {@link Writer} follows the same implementation implications of {@link StringWriter},
 * {@link #close() closing} or {@link #flush() flushing} it does nothing more than updating the {@link #box},
 * but the {@link Writer} will still accept further calls to {@link #write(char[], int, int)}.</p>
 */
public class StringBoxWriter extends Writer {

    private final IMutableBox<String> box;
    private final StringWriter sw = new StringWriter();

    public StringBoxWriter(IMutableBox<String> box) {
        this.box = box;
    }

    @Override
    public void write(@NotNull char[] cbuf, int off, int len) {
        this.sw.write(cbuf, off, len);
    }

    @Override
    public void write(int c) {
        this.sw.write(c);
    }

    @Override
    public void write(@NotNull char[] cbuf) throws IOException {
        this.sw.write(cbuf);
    }

    @Override
    public void write(@NotNull String str) {
        this.sw.write(str);
    }

    @Override
    public void write(@NotNull String str, int off, int len) {
        this.sw.write(str, off, len);
    }

    @Override
    public Writer append(CharSequence csq) throws IOException {
        return this.sw.append(csq);
    }

    @Override
    public Writer append(CharSequence csq, int start, int end) throws IOException {
        return this.sw.append(csq, start, end);
    }

    @Override
    public Writer append(char c) throws IOException {
        return this.sw.append(c);
    }

    @Override
    public void flush() {
        this.sw.flush();
        this.box.set(this.sw.toString());
    }

    @Override
    public void close() throws IOException {
        this.sw.close();
        this.box.set(this.sw.toString());
    }
}
